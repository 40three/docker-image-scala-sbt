Scala SBT Docker-Image
==================

An image to build scala programs. Use 40three/scala-sbt:2.12 for an image based
on scala-2.12.

Updating to a new version
-------------------------

Update the versions-file and commit.

License
-------
This code is open source software licensed under the [Apache 2.0 License]("http://www.apache.org/licenses/LICENSE-2.0.html").
