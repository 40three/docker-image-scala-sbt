#
# Scala and sbt Dockerfile
#

# Pull base image
FROM openjdk:8
ARG scala_version
ARG sbt_version

# install scala

ENV SCALA_VERSION=$scala_version
ENV SBT_VERSION=$sbt_version
WORKDIR /scala
ENV SCALA_HOME /scala/scala-$SCALA_VERSION

RUN \
  curl -s https://downloads.lightbend.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz -o /tmp/scala.tgz && \
  tar -xzf /tmp/scala.tgz && \
  rm /tmp/scala.tgz

ENV PATH="$SCALA_HOME/bin:${PATH}"

# Install sbt
RUN \
  curl -sL http://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb  -o sbt-$SBT_VERSION.deb && \
  dpkg -i sbt-$SBT_VERSION.deb && \
  rm sbt-$SBT_VERSION.deb && \
  sbt sbtVersion

# Define working directory
WORKDIR /root
