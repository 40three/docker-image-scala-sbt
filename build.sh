#!/usr/bin/env bash
#
# builds  pushes the docker image
# specify command "build" to build and "push" to push
set +x

source versions

export SCALA_VERSION=${SCALA_MAJOR_VERSION}.${SCALA_MINOR_VERSION}
export IMAGE_NAME=40three/scala-sbt
export IMAGE_NAME_MAJOR=${IMAGE_NAME}:$SCALA_MAJOR_VERSION
export IMAGE_NAME_MINOR=${IMAGE_NAME}:$SCALA_VERSION


function build {
  echo "starting build of scala-$SCALA_VERSION"
  # build the Docker image (this will use the Dockerfile in the root of the repo)
  docker build --build-arg scala_version=$SCALA_VERSION --build-arg sbt_version=$SBT_VERSION -t $IMAGE_NAME_MAJOR -t $IMAGE_NAME_MINOR .
}

function push {
  echo "starting push of $IMAGE_NAME_MAJOR and $IMAGE_NAME_MINOR"
  # authenticate with the Docker Hub
  docker login --username $DOCKER_HUB_USER --password $DOCKER_HUB_PASSWORD
  # push the new Docker image to the Docker registry
  docker push $IMAGE_NAME_MAJOR
  docker push $IMAGE_NAME_MINOR
}

_WILL_BUILD=false
_WILL_PUSH=false

for i in "$@" ; do
  if [[ $i == "build" ]] ; then
    echo "will build"
    _WILL_BUILD=true
  elif [[ $i == "push" ]]; then
    echo "will push"
    _WILL_PUSH=true
  fi
done

$_WILL_BUILD && build
$_WILL_PUSH && push
